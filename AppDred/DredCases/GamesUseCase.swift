//
//  GamesUseCase.swift
//  AppDred
//
//  Created by Amit Sen on 24/4/18.
//  Copyright © 2018 Amit Sen. All rights reserved.
//

import Foundation
import RxSwift

class GamesUseCase {
    private let network: GamesNetwork
    
    init(network: GamesNetwork) {
        self.network = network
    }
    
    func games() -> Observable<[Game]> {
        return network.fetchGames().asObservable()
    }
}
