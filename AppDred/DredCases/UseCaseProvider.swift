//
//  UseCaseProvider.swift
//  AppDred
//
//  Created by Amit Sen on 24/4/18.
//  Copyright © 2018 Amit Sen. All rights reserved.
//

import Foundation

protocol UseCaseProviderProtocol {
    func makeGamesUseCase() -> GamesUseCase
}

public final class UseCaseProvider: UseCaseProviderProtocol {
    
    private let networkProvider: NetworkProvider
    
    init() {
        networkProvider = NetworkProvider()
    }
    
    // UseCaseProviderProtocol
    func makeGamesUseCase() -> GamesUseCase {
        return GamesUseCase(network: networkProvider.makeGamesNetwork())
    }
}
