//
//  File.swift
//  AppDred
//
//  Created by Amit Sen on 24/4/18.
//  Copyright © 2018 Amit Sen. All rights reserved.
//

import ObjectMapper

class Game: Mappable {
    internal var gameName: String!
    internal var gameId: String!
    internal var playUrl: String!
    internal var launchLocale: String!
    internal var imageUrl: String!
    internal var backgroundImageUrl: String!
    internal var tags: [String]!
    internal var vendorId: String!
    
    required internal init(map: Map) {
        mapping(map: map)
    }
    
    internal func mapping(map: Map) {
        gameName <- map["gameName"]
        gameId <- map["gameId"]
        playUrl <- map["playUrl"]
        launchLocale <- map["launchLocale"]
        imageUrl <- map["imageUrl"]
        backgroundImageUrl <- map["backgroundImageUrl"]
        tags <- map["tags"]
        vendorId <- map["vendorId"]
    }
}
