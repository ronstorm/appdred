//
//  GamesNavigator.swift
//  AppDred
//
//  Created by Amit Sen on 4/25/18.
//  Copyright © 2018 Amit Sen. All rights reserved.
//

import UIKit

protocol GamesNavigator {
    func toGames()
}

class DefaultGamesNavigator: GamesNavigator {
    
    private let navigationController: UINavigationController
    private let services: UseCaseProvider
    
    init(services: UseCaseProvider, navigationController: UINavigationController) {
        self.services = services
        self.navigationController = navigationController
    }
    
    // GamesNavigatorProtocol
    func toGames() {
        let vc = GamesViewController()
        vc.viewModel = GamesViewModel(useCase: services.makeGamesUseCase(),
                                      navigator: self)
        navigationController.pushViewController(vc, animated: true)
    }
}
