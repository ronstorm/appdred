//
//  GamesViewModel.swift
//  
//
//  Created by Amit Sen on 4/25/18.
//

import Foundation
import RxSwift
import RxCocoa

class GamesViewModel: ViewModelProtocol {
    
    struct Input {
        let trigger: Driver<Void>
    }
    
    struct Output {
        let fetching: Driver<Bool>
        let games: Driver<[GameItemViewModel]>
        let error: Driver<Error>
    }
    
    private let useCase: GamesUseCase
    private let navigator: GamesNavigator
    
    init(useCase: GamesUseCase, navigator: GamesNavigator) {
        self.useCase = useCase
        self.navigator = navigator
    }
    
    func transform(input: Input) -> Output {
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        
        let games = input.trigger.flatMapLatest {
            return self.useCase.games()
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
                .map {
                    $0.map {
                        GameItemViewModel(with: $0, useCase: self.useCase)
                    }
                }
        }
        
        let fetching = activityIndicator.asDriver()
        let errors = errorTracker.asDriver()
        
        return Output(fetching: fetching,
                      games: games,
                      error: errors)
        
    }
}
