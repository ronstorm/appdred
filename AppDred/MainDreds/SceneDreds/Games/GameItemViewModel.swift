//
//  GameItemViewModel.swift
//  AppDred
//
//  Created by Amit Sen on 4/25/18.
//  Copyright © 2018 Amit Sen. All rights reserved.
//

import Foundation

final class GameItemViewModel {
    let title:String
    let subtitle : String
    let imageUrl: String
    let useCase: GamesUseCase
    
    init (with game: Game, useCase: GamesUseCase) {
        self.imageUrl = game.imageUrl
        self.title = game.gameName
        self.subtitle = game.vendorId
        self.useCase = useCase
    }
}
