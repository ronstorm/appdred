//
//  GamesViewController.swift
//  AppDred
//
//  Created by Amit Sen on 4/25/18.
//  Copyright © 2018 Amit Sen. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class GamesViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    private let disposeBag = DisposeBag()
    var viewModel: GamesViewModel!
    
    lazy var collectionView: UICollectionView = {
        var cv = UICollectionView(frame: CGRect.zero, collectionViewLayout: self.flowLayout)
        cv.delegate = self
        cv.backgroundColor = UIColor(red: 249/255, green: 249/255, blue: 249/255, alpha: 1.0)
        cv.alwaysBounceVertical = true
        cv.alwaysBounceHorizontal = false
        cv.register(GameCollectionViewCell.self, forCellWithReuseIdentifier: GameCollectionViewCell.reuseID)
        cv.refreshControl = UIRefreshControl()
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()
    
    lazy var flowLayout: UICollectionViewFlowLayout = {
        var flow = UICollectionViewFlowLayout()
        flow.sectionInset = UIEdgeInsetsMake(2.0, 2.0, 2.0, 2.0)
        return flow
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationItem.title = "Games"
        view.backgroundColor = .white
        view.addSubview(collectionView)
        view.addConstraintsWithFormat(visualFormat: "H:|[v0]|", forViews: collectionView)
        view.addConstraintsWithFormat(visualFormat: "V:|-20-[v0]|", forViews: collectionView)
        bindViewModel()
    }
    
    private func bindViewModel() {
        assert(viewModel != nil)
        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
                            .mapToVoid()
                            .asDriverOnErrorJustComplete()
        let pull = collectionView.refreshControl!.rx
                .controlEvent(.valueChanged)
                .asDriver()
        
        let input = GamesViewModel.Input(trigger: Driver.merge(viewWillAppear, pull))
        let output = viewModel.transform(input: input)
        
        // Bind Posts to UITableView
        output.games.drive(collectionView.rx.items(cellIdentifier: GameCollectionViewCell.reuseID, cellType: GameCollectionViewCell.self)) { (_, vm, cell) in
            cell.bind(vm)
        }
    }
    
    // UICollectionViewDelegateFlowLayout
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        self.flowLayout.invalidateLayout()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.bounds.width, height: 100)
    }
}
