//
//  GameCollectionViewCell.swift
//  AppDred
//
//  Created by Amit Sen on 4/25/18.
//  Copyright © 2018 Amit Sen. All rights reserved.
//

import UIKit

class GameCollectionViewCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Game Title"
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let vendorLabel: UILabel = {
        let label = UILabel()
        label.text = "Vendor ID"
        label.font = UIFont.systemFont(ofSize: 15)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let imageView: UIImageView = {
        let imgView = UIImageView()
        imgView.translatesAutoresizingMaskIntoConstraints = false
        return imgView
    }()
    
    func setupViews() {
        backgroundColor = .white
        
        addSubview(titleLabel)
        addSubview(vendorLabel)
        addSubview(imageView)
        
        addConstraintsWithFormat(visualFormat: "H:|-20-[v0(80)]-10-[v1]-20-|", forViews: imageView, titleLabel)
        addConstraintsWithFormat(visualFormat: "H:|-20-[v0]-10-[v1]-20-|", forViews: imageView, vendorLabel)
        addConstraintsWithFormat(visualFormat: "V:|-20-[v0(60)]|", forViews: imageView)
        addConstraintsWithFormat(visualFormat: "V:|-20-[v0(20)][v1(20)]-20-|", forViews: titleLabel, vendorLabel)
        
    }
    
    func bind(_ viewModel: GameItemViewModel) {
        self.titleLabel.text = viewModel.title
        self.vendorLabel.text = viewModel.subtitle
        self.imageView.imageFromUrl(urlString: viewModel.imageUrl)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
