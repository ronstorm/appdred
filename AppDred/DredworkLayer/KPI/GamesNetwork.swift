//
//  GamesNetwork.swift
//  AppDred
//
//  Created by Amit Sen on 24/4/18.
//  Copyright © 2018 Amit Sen. All rights reserved.
//

import RxSwift

public final class GamesNetwork {
    private let network: Network<Game>
    
    init(network: Network<Game>) {
        self.network = network
    }
    
    func fetchGames() -> Observable<[Game]> {
        return network.getItems("getGamesByMarketAndDevice.json?jurisdiction=UK&brand=unibet&deviceGroup=mobilephone&locale=en_GB&currency=GBP&categories=casino,softgames&clientId=casinoapp")
    }
}
