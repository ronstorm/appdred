//
//  Network.swift
//  AppDred
//
//  Created by Amit Sen on 24/4/18.
//  Copyright © 2018 Amit Sen. All rights reserved.
//

import Foundation
import Alamofire
import RxAlamofire
import RxSwift
import ObjectMapper

final class Network<T: Mappable> {
    private let endPoint: String
    private let scheduler: ConcurrentDispatchQueueScheduler
    
    init(_ endPoint: String) {
        self.endPoint = endPoint
        self.scheduler = ConcurrentDispatchQueueScheduler(qos: DispatchQoS(qosClass: DispatchQoS.QoSClass.background, relativePriority: 1))
    }
    
    func getItems(_ path: String) -> Observable<[T]> {
        let absolutePath = "\(endPoint)\(path)"
        return RxAlamofire
            .json(.get, absolutePath)
            .debug()
            .observeOn(scheduler)
            .map({ (json) -> [T] in
                
                let jsonData = json as! [String: Any]
                let gamesJson = jsonData["games"] as! [String: Any]
                var arr: [[String: Any]] = []
                for value in gamesJson.values {
                    arr.append(value as! [String : Any])
                }
                return Mapper<T>().mapArray(JSONObject: arr)!
            })
    }
}
