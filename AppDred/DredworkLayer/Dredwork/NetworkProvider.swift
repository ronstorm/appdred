//
//  NetworkProvider.swift
//  AppDred
//
//  Created by Amit Sen on 24/4/18.
//  Copyright © 2018 Amit Sen. All rights reserved.
//

import Foundation

final class NetworkProvider {
    private let apiEndpoint: String
    
    public init() {
        apiEndpoint = "https://api.unibet.com/game-library-rest-api/"
    }
    
    public func makeGamesNetwork() -> GamesNetwork {
        let network = Network<Game>(apiEndpoint)
        return GamesNetwork(network: network)
    }
}
