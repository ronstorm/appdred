//
//  Application.swift
//  AppDred
//
//  Created by Amit Sen on 24/4/18.
//  Copyright © 2018 Amit Sen. All rights reserved.
//

import UIKit

final class Application {
    static let shared = Application()
    
    private let networkUseCaseProvider: UseCaseProvider
    
    private init() {
        self.networkUseCaseProvider = UseCaseProvider()
    }
    
    func configureMainInterface(in window: UIWindow) {
        
        let gamesNavigationController = UINavigationController()
        let gamesNavigator = DefaultGamesNavigator(services: networkUseCaseProvider,
                                                     navigationController: gamesNavigationController)
        
        window.rootViewController = gamesNavigationController
        window.makeKeyAndVisible()
        
        gamesNavigator.toGames()
    }
}
