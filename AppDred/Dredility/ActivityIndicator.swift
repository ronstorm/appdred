//
//  ActivityIndicator.swift
//  AppDred
//
//  Created by Amit Sen on 4/24/18.
//  Copyright © 2018 Amit Sen. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

public class ActivityIndicator: SharedSequenceConvertibleType {
    public typealias E = Bool
    
    private let _lock = NSRecursiveLock()
    private let _variable = Variable(false)
    private let _loading: SharedSequence<DriverSharingStrategy, Bool>
    
    public init() {
        _loading = _variable
            .asDriver()
            .distinctUntilChanged()
    }
    
    public func subscribed() {
        _lock.lock()
        _variable.value = true
        _lock.unlock()
    }
    
    public func sendStopLoading() {
        _lock.lock()
        _variable.value = false
        _lock.unlock()
    }
    
    fileprivate func trackActivityOfObservable<O: ObservableConvertibleType>(_ source: O) -> Observable<O.E> {
        return source.asObservable()
            .do(onNext: { (_) in
                self.sendStopLoading()
            }, onError: { (_) in
                self.sendStopLoading()
            }, onCompleted: {
                self.sendStopLoading()
            }, onSubscribe: subscribed, onSubscribed: nil, onDispose: nil)
    }
    
    public func asSharedSequence() -> SharedSequence<DriverSharingStrategy, E> {
        return _loading
    }
}

extension ObservableConvertibleType {
    public func trackActivity(_ activityIndicator: ActivityIndicator) -> Observable<E> {
        return activityIndicator.trackActivityOfObservable(self)
    }
}
