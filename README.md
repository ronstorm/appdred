# AppDred

AppDred is an app made for the Test Purpose asked by Kindred.

## Problem

- Build a simple app that lists games images and titles, based on data from [this url](https://api.unibet.com/game-library-rest-api/getGamesByMarketAndDevice.json?jurisdiction=UK&brand=unibet&deviceGroup=mobilephone&locale=en_GB&currency=GBP&categories=casino,softgames&clientId=casinoapp).

- The project should be version controlled by git, so that we can see how you worked out the problem in git history.

- Create a readMe file and explain your design considerations.

- To the readMe file, also add some thoughts about what you would design/implement if you could spend more time on the project.

### Prerequisites

- A Mac computer with Xcode installed.

### Pods needed

* [RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [RxCocoa](https://github.com/ReactiveX/RxSwift/tree/master/RxCocoa): Extension of RxSwift
* [Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift.
* [RxAlamofire](https://github.com/RxSwiftCommunity/RxAlamofire): RxSwift wrapper around the elegant HTTP networking in Swift Alamofire.
* [ObjectMapper](https://github.com/Hearst-DD/ObjectMapper): Simple JSON Object mapping written in Swift.
* [AlamofireObjectMapper](https://github.com/tristanhimmelman/AlamofireObjectMapper): An Alamofire extension which converts JSON response data into swift objects using ObjectMapper.

### Design Pattern

I used [MVVM](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93viewmodel) pattern.

### Workflow

* The AppDred starts with a singleton class called `Application`. It actually initializes the `Navigator`, the `UseCaseProvider` and the root navigation controller.

* `Navigator` takes care of all the navigations from the current scene. It passes the data to `ViewModel` of the `ViewController`, which we want to navigate to.

* Now `ViewModel` does the main job for us. It asks the `Network` module to fetch the necessary data through `UseCaseProvider`.

* `Network` is a generic class which is ready to be implemented for all CRUD operation through APIs.

* `Models` are the data classes we use to hold the data coming through the `Network` module.

* Finally `ViewController` is responsible to show the view.

## Running the tests

ViewModelTests include the test cases we need to test.

## BitBucket Link

* [AppDred](https://bitbucket.org/ronstorm/appdred/) - The source and git versioning of AppDred

## Authors

* **Amit Sen** - *Complete Work* - [ronstorm](https://github.com/ronstorm)
