//
//  GamesViewModelTests.swift
//  AppDredTests
//
//  Created by Amit Sen on 26/4/18.
//  Copyright © 2018 Amit Sen. All rights reserved.
//

@testable import AppDred
import XCTest
import RxSwift
import RxCocoa

enum TestError: Error {
    case test
}

class GamesViewModelTests: XCTestCase {
    var gamesUseCase: GamesUseCaseMock!
    var gamesNavigator: GameNavigatorMock!
    var viewModel: GamesViewModel!
    
    let disposeBag = DisposeBag()
    
    override func setUp() {
        super.setUp()
        
        gamesUseCase = GamesUseCaseMock(network: GamesNetwork(network: Network<Game>("https://api.unibet.com/game-library-rest-api/")))
        gamesNavigator = GameNavigatorMock()
        
        viewModel = GamesViewModel(useCase: gamesUseCase,
                                   navigator: gamesNavigator)
    }
    
    func test_transform_triggerInvoked_gameEmitted() {
        // arrange
        let trigger = PublishSubject<Void>()
        let input = createInput(trigger: trigger)
        let output = viewModel.transform(input: input)
        
        // act
        output.games.drive().disposed(by: disposeBag)
        trigger.onNext(())
        
        // assert
        XCTAssert(gamesUseCase.games_Called)
    }
    
    private func createInput(trigger: Observable<Void> = Observable.just(()))
        -> GamesViewModel.Input {
            return GamesViewModel.Input(
                trigger: trigger.asDriverOnErrorJustComplete())
    }
}
