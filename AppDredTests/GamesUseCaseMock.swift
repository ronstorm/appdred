//
//  GamesUseCaseMock.swift
//  AppDredTests
//
//  Created by Amit Sen on 26/4/18.
//  Copyright © 2018 Amit Sen. All rights reserved.
//

@testable import AppDred
import RxSwift

class GamesUseCaseMock: GamesUseCase {
    var games_ReturnValue: Observable<[Game]> = Observable.just([])
    var games_Called = false
    
    override func games() -> Observable<[Game]> {
        games_Called = true
        return games_ReturnValue
    }
}
