//
//  GameNavigatorMock.swift
//  AppDredTests
//
//  Created by Amit Sen on 26/4/18.
//  Copyright © 2018 Amit Sen. All rights reserved.
//

@testable import AppDred
import RxSwift


class GameNavigatorMock: GamesNavigator {
    var toGames_Called = false
    
    func toGames() {
        toGames_Called = true
    }
    
}
